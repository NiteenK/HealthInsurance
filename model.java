public class model {

	public static float getQuote(applicantBean h) {
		float basePremium=5000;
		if(h.getAge()>=18){
			basePremium+=basePremium*10/100;
		}
		if(h.getAge()>=25){
			basePremium+=basePremium*10/100;
		}
		if(h.getAge()>=30){
			basePremium+=basePremium*10/100;
		}
		if(h.getAge()>=35){
			basePremium+=basePremium*10/100;
		}
		if(h.getAge()>=40){
			int afterVal=h.getAge()-40;
			while(afterVal>0){
				basePremium+=basePremium*20/100;
				afterVal=afterVal-5;
			}
		}
		//code need to write for 40+age;
		if(h.getGender().equals("Male")){
			basePremium+=basePremium*2/100;
		}
		if(h.getHealth().isBloodPressure()){
			basePremium+=basePremium*1/100;
		}
		if(h.getHealth().isBloodSugar()){
			basePremium+=basePremium*1/100;
		}
		if(h.getHealth().isHypertension()){
			basePremium+=basePremium*1/100;
		}
		if(h.getHealth().isOverWeight()){
			basePremium+=basePremium*1/100;
		}
		if(h.getHabits().isDialyExercise()){
			basePremium-=(basePremium*3/100);
		}
		if(h.getHabits().isAlchohol()){
			basePremium+=basePremium*3/100;
		}
		if(h.getHabits().isDrugs()){
			basePremium+=basePremium*3/100;
		}
		if(h.getHabits().isSmoking()){
			basePremium+=basePremium*3/100;
		}
		return basePremium;
		
	}
	

}